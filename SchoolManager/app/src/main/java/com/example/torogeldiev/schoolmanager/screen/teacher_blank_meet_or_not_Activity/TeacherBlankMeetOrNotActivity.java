package com.example.torogeldiev.schoolmanager.screen.teacher_blank_meet_or_not_Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.torogeldiev.schoolmanager.R;
import com.example.torogeldiev.schoolmanager.adapter.RecyclerAdapterViewContactMeet;
import com.example.torogeldiev.schoolmanager.api.OnLoadMoreListener;
import com.example.torogeldiev.schoolmanager.entity.contact_person.ContactPerson;
import com.example.torogeldiev.schoolmanager.entity.create_meet_with_contact_person.response_body.TotalResponseForCreateMeet;
import com.example.torogeldiev.schoolmanager.entity.object.Objec;
import com.example.torogeldiev.schoolmanager.screen.map.MapActivity;
import com.example.torogeldiev.schoolmanager.screen.teacher_blank_meet_or_not_Activity.create_meet_activity.CreateMeetActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TeacherBlankMeetOrNotActivity extends AppCompatActivity implements
        View.OnClickListener, TeacherBlankMeetOrNotActivityView, OnLoadMoreListener {

    //Toolbar
    @BindView(R.id.toolbar_search_visible)
    Toolbar toolbar;
    @BindView(R.id.ll_toolbar_subtitle_visible_for_search)
    LinearLayout llSearch;
    @BindView(R.id.toolbar_subtitle_visible_iv_search)
    ImageView btnSearchVisible;
    @BindView(R.id.toolbar_subtitle_visible_iv_filter)
    ImageView btnFilter;
    @BindView(R.id.toolbar_subtitle_visible_iv_add)
    ImageView btnAddNew;
    @BindView(R.id.toolbar_subtitle_visible_edit)
    EditText etSearch;
    @BindView(R.id.toolbar_subtitle_visible_close)
    ImageView closeSearch;
    @BindView(R.id.toolbar_subtitle_visible_title)
    TextView titleToolbar;

    @BindView(R.id.iv_teacher_blank_btn_map)
    ImageView btnMap;

    @BindView(R.id.tv_teacher_blank_btn_meeted)
    TextView btnMeeted;
    @BindView(R.id.tv_teacher_blank_btn_not_meeted)
    TextView btnNotMeeted;
    @BindView(R.id.tv_teacher_blank_title_meet_or_not)
    TextView titleMeetOrNot;
    @BindView(R.id.rv_teacher_blank)
    RecyclerView recyclerView;

    @BindView(R.id.tv_teacher_blank_org_name)
    TextView nameOrganization;
    @BindView(R.id.tv_teacher_blank_org_telephone)
    TextView phoneOrganization;
    @BindView(R.id.tv_teacher_blank_org_address)
    TextView cityOrganization;
    @BindView(R.id.tv_teacher_blank_contact_name)
    TextView contactName;

    RecyclerAdapterViewContactMeet adapterRecyclerViewContactMeetings;

    int managerId, contactPersonId, partnerId;

    private int page = 1;
    private int size = 100;
    String contactPerson;
    String status;

    //Values for loading more
    private LinearLayoutManager linearLayoutManagerRe;
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    boolean isLoaded = false;
    boolean sizeNull = false;

    String organization;
    String telephone;
    String city;
    String lng;
    String lat;

    private TeacherBlankMeetOrNotActivityPresenter presenter;
    private List<Objec> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_blank_meet_or_not);

        ButterKnife.bind(this);

        btnFilter.setVisibility(View.GONE);
        btnAddNew.setVisibility(View.VISIBLE);

        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        managerId = getIntent().getIntExtra("managerId", managerId);
        contactPersonId = getIntent().getIntExtra("contactPersonId", contactPersonId);
        partnerId = getIntent().getIntExtra("partnerId", partnerId);

        titleToolbar.setTextColor(getResources().getColor(android.R.color.white));
        titleToolbar.setText("Встречи");

        organization = getIntent().getStringExtra("organization");
        telephone = getIntent().getStringExtra("telephone");
        city = getIntent().getStringExtra("city");
        lng = getIntent().getStringExtra("longitude");
        lat = getIntent().getStringExtra("latitude");

        nameOrganization.setText(organization);
        phoneOrganization.setText(telephone);
        cityOrganization.setText(city);
        contactName.setText(getIntent().getStringExtra("object"));

        contactPerson = String.valueOf(contactPersonId);
        status = "1";

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        linearLayoutManagerRe = (LinearLayoutManager) recyclerView.getLayoutManager();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!sizeNull) {
                    totalItemCount = linearLayoutManagerRe.getItemCount();
                    lastVisibleItem = linearLayoutManagerRe.findLastVisibleItemPosition();
                    if (!isLoaded && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (adapterRecyclerViewContactMeetings
                                .getOnLoadMoreListener() != null) {
                            adapterRecyclerViewContactMeetings
                                    .getOnLoadMoreListener().onLoadMore();
                        }
                        isLoaded = true;
                    }
                }

            }
        });

        presenter = new TeacherBlankMeetOrNotActivityPresenter(this, this);
        titleMeetOrNot.setText("Назначенные встречи");
        presenter.getListPlannedContacts(contactPerson, status, page, size);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnSearchVisible.setVisibility(View.GONE);
        btnAddNew.setOnClickListener(this);
        closeSearch.setOnClickListener(this);
        btnMeeted.setOnClickListener(this);
        btnNotMeeted.setOnClickListener(this);
        btnMap.setOnClickListener(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_subtitle_visible_close:
                llSearch.setVisibility(View.GONE);
                break;
            case R.id.toolbar_subtitle_visible_iv_add:
                Intent intent = new Intent(TeacherBlankMeetOrNotActivity.this,
                        CreateMeetActivity.class);
                intent.putExtra("partnerIdFromTeacherBlank", partnerId);
                intent.putExtra("contactPersonIdFromTeacherBlank", contactPersonId);
                startActivity(intent);
                break;
            case R.id.tv_teacher_blank_btn_meeted:
                titleMeetOrNot.setText("Назначенные встречи");
                presenter.getListPlannedContacts(contactPerson,
                        "1", 1, size);
                break;
            case R.id.tv_teacher_blank_btn_not_meeted:
                //adapterRecyclerViewContactMeetings.notifyDataSetChanged();
                titleMeetOrNot.setText("Пройденные встречи");
                presenter.getListPlannedContacts(contactPerson,
                        "2", 1, size);
                break;
            //buttons map/phone
            case R.id.iv_teacher_blank_btn_map:
                Intent inten = new Intent(TeacherBlankMeetOrNotActivity.this, MapActivity.class);
                inten.putExtra("city", city);
                inten.putExtra("nameOrganization", organization);
                inten.putExtra("telephone", telephone);
                inten.putExtra("longitude", lng);
                inten.putExtra("latitude", lat);
                startActivity(inten);
                break;
        }
    }

    public void getListPlannedContacts(ContactPerson person) {
        list = new ArrayList<>();
        list.addAll(person.getObjects());
        if (list.size() == 0) {
            list.clear();
            adapterRecyclerViewContactMeetings =
                    new RecyclerAdapterViewContactMeet(this, list);
            recyclerView.setAdapter(adapterRecyclerViewContactMeetings);
            adapterRecyclerViewContactMeetings.setOnLoadMoreListener(this);
            recyclerView.setNestedScrollingEnabled(false);
            adapterRecyclerViewContactMeetings.notifyDataSetChanged();
            Toast.makeText(this, "Пустой список", Toast.LENGTH_SHORT).show();
        } else {
            adapterRecyclerViewContactMeetings =
                    new RecyclerAdapterViewContactMeet(this, list);
            recyclerView.setAdapter(adapterRecyclerViewContactMeetings);
            adapterRecyclerViewContactMeetings.setOnLoadMoreListener(this);
            recyclerView.setNestedScrollingEnabled(false);
            adapterRecyclerViewContactMeetings.notifyDataSetChanged();
        }
    }

    @Override
    public void getingListPlannedContactsLoadMore(ContactPerson contactPerson) {
        if (contactPerson.getObjects().size() != 0) {
            list.remove(list.size() - 1);
            adapterRecyclerViewContactMeetings.notifyItemRemoved(list.size());
            list.addAll(contactPerson.getObjects());
            adapterRecyclerViewContactMeetings.setList(list);
            adapterRecyclerViewContactMeetings.notifyDataSetChanged();
        }else{
            sizeNull = true;
            Toast.makeText(this, "Больше нет данных!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void getCRC() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1000 && resultCode == RESULT_OK){
            titleMeetOrNot.setText("Назначенные встречи");
            presenter.getListPlannedContacts(String.valueOf(contactPersonId), "1", 1, 10);
        }
    }

    @Override
    public void onLoadMore() {
        adapterRecyclerViewContactMeetings.getList().add(null);
        adapterRecyclerViewContactMeetings
                .notifyItemInserted(adapterRecyclerViewContactMeetings.getList().size() - 1);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                adapterRecyclerViewContactMeetings
                        .getList()
                        .remove(adapterRecyclerViewContactMeetings.getList().size() - 1);
                adapterRecyclerViewContactMeetings
                        .notifyItemRemoved(adapterRecyclerViewContactMeetings.getList().size());
                page++;
                getNextGsonObject(contactPerson, status, page, size);
                isLoaded = false;
            }
        }, 4000);
    }

    private void getNextGsonObject(String contactPerson, String status,int os, int lim) {
//        gsonList.add(new GsonObjectFilter("responsible.id", "=", String.valueOf(user.getId())));
        presenter.getListPlannedContactsLoadMore(contactPerson, status, os, lim);
    }
}

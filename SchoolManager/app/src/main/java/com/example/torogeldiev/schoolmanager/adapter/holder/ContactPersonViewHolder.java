package com.example.torogeldiev.schoolmanager.adapter.holder;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.torogeldiev.schoolmanager.R;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by torogeldiev on 15.02.2018.
 */

public class ContactPersonViewHolder extends RecyclerView.ViewHolder{

    @BindDrawable(R.drawable.circle_not_active)
    public Drawable notActive;
    @BindDrawable(R.drawable.circleactive)
    public Drawable active;

    @BindView(R.id.iv_item_list_contacts_star)
    public ImageView badge;
    @BindView(R.id.tv_item_list_contacts_name_teacher)
    public TextView nameTeacher;
    @BindView(R.id.tv_item_list_contacts_address)
    public TextView address;
    @BindView(R.id.tv_item_list_contacts_telephone)
    public TextView telephone;
    @BindView(R.id.btn_phone)
    public ImageView image_phone;
    @BindView(R.id.tv_item_list_contacts_org)
    public TextView organization;


    public ContactPersonViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

package com.example.torogeldiev.schoolmanager.api;

import com.example.torogeldiev.schoolmanager.entity.contact_person.ContactPerson;
import com.example.torogeldiev.schoolmanager.entity.contact_person.CreateContatcPerson;
import com.example.torogeldiev.schoolmanager.entity.create_meet_with_contact_person.CreateMeetWithContactPerson;
import com.example.torogeldiev.schoolmanager.entity.create_meet_with_contact_person.response_body.TotalResponseForCreateMeet;
import com.example.torogeldiev.schoolmanager.entity.finish_meet_with_contact_person.FinishMeetWithContactPerson;
import com.example.torogeldiev.schoolmanager.entity.object.Objec;
import com.example.torogeldiev.schoolmanager.entity.subjects.Subject;
import com.example.torogeldiev.schoolmanager.entity.subjects.response_subjects.ListSubjects;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by torogeldiev on 15.02.2018.
 */

public interface UserLogined {
    @GET("api/contact_persons")
    Call<ContactPerson> getContactPerson(@Query("page") int page, @Query("size") int size);

    /**
     *
     get list of contactpersons with filter cityId
     */
    @GET("api/contact_persons")
    Call<ContactPerson> getContactPersonWithFilter(@Query("page") int page,
                                                   @Query("size") int size,
                                                   @Query("city") String city,
                                                   @Query("country") String country,
                                                   @Query("region") String region);

    @POST("api/contact_person/create")
    Call<Objec> createContactPerson(@Body CreateContatcPerson newPerson);

    @GET("api/countries")
    Call<ContactPerson> getCountries(@Query("page") int page, @Query("size") int size);

    @GET("api/regions")
    Call<ContactPerson> getRegions(@Query("page") int page, @Query("size") int size, @Query("country") String idcountry);

    @GET("api/cities")
    Call<ContactPerson> getCities(@Query("page") int page, @Query("size") int size, @Query("region") String idregion);

    @GET("api/partners")
    Call<ContactPerson> getPartners(@Query("page") int page, @Query("size") int size, @Query("city") String city);


    /**
     * для создания встречи
     */
    @POST("api/contact/create")
    Call<TotalResponseForCreateMeet> createMeet(@Body CreateMeetWithContactPerson createMeetWithContactPerson);

    /**
     * для завершения встречи
     */
    @POST("api/contact/finish")
    Call<TotalResponseForCreateMeet> finishMeet(@Body FinishMeetWithContactPerson finishMeetWithContactPerson);

    /**
     * для получения списка предметов встреч
     */
    @GET("api/subjects")
    Call<ListSubjects> getListSubjects(@Query("page") int page, @Query("size") int size);

    /**
     * для получения списка встреч по фильтру статуса
     */
    @GET("api/contacts")
    Call<ContactPerson> getListContacts(@Query("contactPerson") String contactPerson,
                                        @Query("status") String status,
                                        @Query("page") int page,
                                        @Query("size") int size);


}
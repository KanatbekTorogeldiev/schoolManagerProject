package com.example.torogeldiev.schoolmanager.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.torogeldiev.schoolmanager.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by torogeldiev on 15.02.2018.
 */

public class TeacherBlankListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_item_list_teacher_blank_date_visit)
    public TextView dateVisit;
    @BindView(R.id.tv_item_list_teacher_blank_show_map)
    public TextView showInMap;

    public TeacherBlankListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

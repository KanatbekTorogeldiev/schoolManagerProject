package com.example.torogeldiev.schoolmanager.entity.subjects.response_subjects;

import com.example.torogeldiev.schoolmanager.entity.subjects.Subject;

import java.util.List;

/**
 * Created by Iolana on 21.02.2018.
 */

//класс для описания объекта, в котором ссылка на список предметов встреч
public class ListSubjects {

    private int count;
    private int page;
    private int size;
    private List<Subject> objects;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<Subject> getObjects() {
        return objects;
    }

    public void setObjects(List<Subject> objects) {
        this.objects = objects;
    }
}

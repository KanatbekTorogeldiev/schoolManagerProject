package com.example.torogeldiev.schoolmanager.entity.subjects;

import com.example.torogeldiev.schoolmanager.entity.application.Application;

/**
 * Created by Iolana on 21.02.2018.
 */

//класс для описания встречи

public class Subject {

    private int id;
    private String name;
    private Application application;
    private String createDate;
    private int status;
    private String statusname;

    public Subject(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusname() {
        return statusname;
    }

    public void setStatusname(String statusname) {
        this.statusname = statusname;
    }
}
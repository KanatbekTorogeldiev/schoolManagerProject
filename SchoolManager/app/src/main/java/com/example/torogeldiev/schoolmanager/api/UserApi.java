package com.example.torogeldiev.schoolmanager.api;

import com.example.torogeldiev.schoolmanager.entity.token_user.TokenUser;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by torogeldiev on 14.02.2018.
 */

public interface UserApi {

    @FormUrlEncoded
    @POST("token")
    Call<TokenUser> authToken(@Header("token") String token , @Field("password") String password,
                              @Field("username") String username , @Field("grant_type") String grant_type);



}

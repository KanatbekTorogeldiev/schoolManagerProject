package com.example.torogeldiev.schoolmanager.entity.contact_person;

import com.example.torogeldiev.schoolmanager.entity.city.City;
import com.example.torogeldiev.schoolmanager.entity.country.Country;
import com.example.torogeldiev.schoolmanager.entity.manager.Manager;
import com.example.torogeldiev.schoolmanager.entity.region.Region;

import java.util.Date;

/**
 * Created by torogeldiev on 19.02.2018.
 */

public class CreateContatcPerson {

    private int cityId;
    private String telephone;
    private String email;
    private String surname;
    private String name;
    private String secondName;
    private String sex;
    private Date birthdate;
    private int managerId;
    private String password;
    private int partnerId;
    private String parameter;
    private String additionalContacts;


    public CreateContatcPerson(int cityId, String telephone, String email, String surname, String name, String secondName, String sex, Date birthdate, int managerId, String password, int partnerId, String parameter, String additionalContacts) {
        this.cityId = cityId;
        this.telephone = telephone;
        this.email = email;
        this.surname = surname;
        this.name = name;
        this.secondName = secondName;
        this.sex = sex;
        this.birthdate = birthdate;
        this.managerId = managerId;
        this.password = password;
        this.partnerId = partnerId;
        this.parameter = parameter;
        this.additionalContacts = additionalContacts;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(int partnerId) {
        this.partnerId = partnerId;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getAdditionalContacts() {
        return additionalContacts;
    }

    public void setAdditionalContacts(String additionalContacts) {
        this.additionalContacts = additionalContacts;
    }
}

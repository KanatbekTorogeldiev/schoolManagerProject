package com.example.torogeldiev.schoolmanager.screen.main_list_activity;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.torogeldiev.schoolmanager.App;
import com.example.torogeldiev.schoolmanager.R;
import com.example.torogeldiev.schoolmanager.adapter.RecyclerAdapterRadioButton;
import com.example.torogeldiev.schoolmanager.entity.city.City;
import com.example.torogeldiev.schoolmanager.entity.contact_person.ContactPerson;
import com.example.torogeldiev.schoolmanager.entity.country.Country;
import com.example.torogeldiev.schoolmanager.entity.object.Objec;
import com.example.torogeldiev.schoolmanager.entity.region.Region;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by torogeldiev on 15.02.2018.
 */

public class MainListActivityPresenter implements View.OnClickListener {

    private Context context;
    private MainListActivityView listActivityView;
    private android.support.v7.app.AlertDialog alertDialogFilter;
    private List<Objec> listCountries = new ArrayList<>();
    private List<Objec> listRegions = new ArrayList<>();
    private List<Objec> listCities = new ArrayList<>();
    private AlertDialog dialogMore;
    private Objec objec;

    TextView tv_county, tv_region, tv_cities, tv_ready, tv_cancel;

    private int countryId;
    private int regionId;
    private int cityId;

    private boolean county = false;
    private boolean region = false;
    private boolean city = false;

    int page = 1;
    int size = 100;

    private RecyclerAdapterRadioButton mAdapter;

    public MainListActivityPresenter(Context context, MainListActivityView listActivityView) {
        this.context = context;
        this.listActivityView = listActivityView;
    }

    void getListPersonContacts(int page, int size) {
        App.getUserLogined().getContactPerson(page, size).enqueue(new Callback<ContactPerson>() {
            @Override
            public void onResponse(Call<ContactPerson> call, Response<ContactPerson> response) {
                if (response.isSuccessful()) {
                    listActivityView.getingListContactPerson(response.body());
                } else {
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ContactPerson> call, Throwable t) {
                Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }


    void getListPersonContactsLoadMore(int page, int size) {
        App.getUserLogined().getContactPerson(page, size).enqueue(new Callback<ContactPerson>() {
            @Override
            public void onResponse(Call<ContactPerson> call, Response<ContactPerson> response) {
                if (response.isSuccessful()) {
                    listActivityView.getingListContactPersonLoadMore(response.body());
                } else {
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ContactPerson> call, Throwable t) {
                Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void getListPersonContactsWithFilter(int page, int size, String cityId, String countryId, String regionId) {
        App.getUserLogined().getContactPersonWithFilter(page, size, cityId != null ? cityId : "",
                countryId != null ? countryId : "",
                regionId != null ? regionId : "")
                .enqueue(new Callback<ContactPerson>() {
                    @Override
                    public void onResponse(Call<ContactPerson> call, Response<ContactPerson> response) {
                        if (response.isSuccessful()) {
                            listActivityView.getingListContactPerson(response.body());
                        } else {
                            Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ContactPerson> call, Throwable t) {
                        Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    void getCountries(int page, int size) {
        App.getUserLogined().getCountries(1, 100).enqueue(new Callback<ContactPerson>() {
            @Override
            public void onResponse(Call<ContactPerson> call, Response<ContactPerson> response) {
                if (response.isSuccessful()) {
                    listCountries.clear();
                    listRegions.clear();
                    listCities.clear();
                    listCountries.addAll(response.body().getObjects());
                    listActivityView.getCRC();
                } else {
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ContactPerson> call, Throwable t) {
                Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void getRegions(String id, int page, int size) {
        App.getUserLogined().getRegions(page, size, id).enqueue(new Callback<ContactPerson>() {
            @Override
            public void onResponse(Call<ContactPerson> call, Response<ContactPerson> response) {
                if (response.isSuccessful() && response.body() != null) {
                    listRegions.clear();
                    listCities.clear();
                    listRegions.addAll(response.body().getObjects());
                    listActivityView.getCRC();
                } else {
                    listRegions.clear();
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ContactPerson> call, Throwable t) {
                Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void getCities(String id, int page, int size) {
        App.getUserLogined().getCities(page, size, id).enqueue(new Callback<ContactPerson>() {
            @Override
            public void onResponse(Call<ContactPerson> call, Response<ContactPerson> response) {
                if (response.isSuccessful() && response.body() != null) {
                    listCities.clear();
                    listCities.addAll(response.body().getObjects());
                    listActivityView.getCRC();
                } else {
                    listCities.clear();
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ContactPerson> call, Throwable t) {
                Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     * Диалоговое окно для фильтра
     */
    public void openDialogFilter(Activity activity) {
        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        View viewDialog = layoutInflater.inflate(R.layout.dialog_filter, (ViewGroup) activity.findViewById(R.id.dialog_filter_country_region_city));
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(viewDialog);
        builder.setCancelable(true);
        initialValues(viewDialog);
        cityId = 0;
        regionId = 0;
        tv_ready.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);
        tv_county.setOnClickListener(this);
        tv_region.setOnClickListener(this);
        tv_cities.setOnClickListener(this);
        dialogMore = builder.create();
        dialogMore.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_dialog_filter_ready:
                listActivityView
                        .resiltFilterReady(countryId != 0 ? String.valueOf(countryId) : "", regionId != 0 ? String.valueOf(regionId) : "", cityId != 0 ? String.valueOf(cityId) : "");
                dialogMore.dismiss();
                break;
            case R.id.btn_dialog_filter_cancel:
                dialogMore.dismiss();
                break;
            case R.id.dialog_filter_country:
                int idCountry = countryId;
                dialogRadio(v.getId(), idCountry);
                break;
            case R.id.dialog_filter_region:
                if (listRegions.size() != 0) {
                    int idRegion = regionId;
                    dialogRadio(v.getId(), idRegion);
                } else {
                    Toast.makeText(context, "В данной стране, регионы отсутствуют!", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.dialog_filter_city:
                if (listCities.size() != 0) {
                    int idCity = cityId;
                    dialogRadio(v.getId(), idCity);
                } else {
                    Toast.makeText(context, "В данной регионе, города отсутствуют!", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    /**
     * Работа с динамичным Диалоговым окном
     */
    void dialogRadio(int id, long idSelected) {
        List<Objec> list = new ArrayList<>();
        switch (id) {
            case R.id.dialog_filter_country:
                list.addAll(listCountries);
                county = true;
                region = false;
                city = false;
                break;
            case R.id.dialog_filter_region:
                list.addAll(listRegions);
                county = false;
                region = true;
                city = false;
                break;
            case R.id.dialog_filter_city:
                list.addAll(listCities);
                county = false;
                region = false;
                city = true;
                break;
        }

        View dialogView = View.inflate(context, R.layout.dialog_coutry_region_city, null);
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        RecyclerView mRecyclerView = dialogView.findViewById(R.id.rv_dialog_c_r_c);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new RecyclerAdapterRadioButton(context, list);
        mAdapter.setSelectId(idSelected);
        mRecyclerView.setAdapter(mAdapter);
        TextView dialready = dialogView.findViewById(R.id.tv_dialog_c_r_c_ready);
        TextView cancel = dialogView.findViewById(R.id.tv_dialog_c_r_c_cancel);
        dialready.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objec = mAdapter.getoObj();

                if (county) {
                    if (objec != null) {
                        countryId = objec.getId();
                        tv_county.setText(objec.getName());
                        tv_county.setTextColor(view.getResources().getColor(android.R.color.black));
                        getRegions(String.valueOf(countryId), page, size);
                        alertDialog.dismiss();
                    } else {
                        Toast.makeText(context, "Выберите страну!", Toast.LENGTH_SHORT).show();
                    }
                } else if (region) {
                    if (objec != null) {
                        regionId = objec.getId();
                        tv_region.setText(objec.getName());
                        tv_region.setTextColor(view.getResources().getColor(android.R.color.black));
                        getCities(String.valueOf(regionId), page, size);
                        alertDialog.dismiss();

                    } else {
                        Toast.makeText(context, "Выберите регион", Toast.LENGTH_SHORT).show();
                    }
                } else if (city) {
                    if (objec != null) {
                        cityId = objec.getId();
                        tv_cities.setText(objec.getName());
                        tv_cities.setTextColor(view.getResources().getColor(android.R.color.black));
                        alertDialog.dismiss();
                    } else {
                        Toast.makeText(context, "Выберите город", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    void initialValues(View view) {
        tv_county = view.findViewById(R.id.dialog_filter_country);
        tv_region = view.findViewById(R.id.dialog_filter_region);
        tv_cities = view.findViewById(R.id.dialog_filter_city);
        tv_ready = view.findViewById(R.id.btn_dialog_filter_ready);
        tv_cancel = view.findViewById(R.id.btn_dialog_filter_cancel);
    }

}

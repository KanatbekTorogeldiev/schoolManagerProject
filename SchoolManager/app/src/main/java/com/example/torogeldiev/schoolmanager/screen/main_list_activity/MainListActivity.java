package com.example.torogeldiev.schoolmanager.screen.main_list_activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.torogeldiev.schoolmanager.R;
import com.example.torogeldiev.schoolmanager.adapter.AdapterRecyclerViewContacts;
import com.example.torogeldiev.schoolmanager.api.OnLoadMoreListener;
import com.example.torogeldiev.schoolmanager.entity.contact_person.ContactPerson;
import com.example.torogeldiev.schoolmanager.entity.object.Objec;
import com.example.torogeldiev.schoolmanager.entity.token_user.TokenUser;
import com.example.torogeldiev.schoolmanager.screen.main_list_activity.new_person.NewPersonActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;

public class MainListActivity extends AppCompatActivity implements View.OnClickListener,
        MainListActivityView, OnLoadMoreListener {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.btn_add_new_contact)
    FloatingActionButton fab;

    //Toolbar
    @BindView(R.id.toolbar_search_visible)
    Toolbar toolbar;
    @BindView(R.id.ll_toolbar_subtitle_visible_for_search)
    LinearLayout llSearch;
    @BindView(R.id.toolbar_subtitle_visible_iv_search)
    ImageView btnSearchVisible;
    @BindView(R.id.toolbar_subtitle_visible_iv_filter)
    ImageView btnFilter;
    @BindView(R.id.toolbar_subtitle_visible_edit)
    EditText etSearch;
    @BindView(R.id.toolbar_subtitle_visible_close)
    ImageView closeSearch;
    @BindView(R.id.toolbar_subtitle_visible_title)
    TextView titleToolbar;

    private MainListActivityPresenter presenter;
    private int page = 1;
    private int size = 100;
    private AdapterRecyclerViewContacts adapterRecyclerViewContacts;
    private List<Objec> list;

    //Values for loading more
    private LinearLayoutManager linearLayoutManagerRe;
    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    boolean isLoaded = false;
    boolean sizeNull = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list);
        ButterKnife.bind(this);

        //Шаблонный проектирование какие пожходы используются

        //Работа с тулбаром
        toolbar.setTitle("");
        TokenUser tokenUser = new TokenUser();
        String nameUser = tokenUser.getUserName();
        titleToolbar.setText("Контактные лица");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        linearLayoutManagerRe = (LinearLayoutManager) recyclerView.getLayoutManager();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!sizeNull) {
                    totalItemCount = linearLayoutManagerRe.getItemCount();
                    lastVisibleItem = linearLayoutManagerRe.findLastVisibleItemPosition();
                    if (!isLoaded && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (adapterRecyclerViewContacts.getOnLoadMoreListener() != null) {
                            adapterRecyclerViewContacts.getOnLoadMoreListener()
                                    .onLoadMore();
                        }
                        isLoaded = true;
                    }
                }

            }
        });
        presenter = new MainListActivityPresenter(this, this);
        presenter.getListPersonContacts(page, size);
        btnSearchVisible.setOnClickListener(this);
        btnFilter.setOnClickListener(this);
        closeSearch.setOnClickListener(this);
        btnFilter.setOnClickListener(this);
        fab.setOnClickListener(this);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                List<Objec> result = Observable.fromIterable(list)
                        .filter(item -> item.getFullName()
                                .toUpperCase()
                                .contains(etSearch.getText().toString().toUpperCase()))
                        .toList()
                        .blockingGet();
                adapterRecyclerViewContacts.setList(result);
                adapterRecyclerViewContacts.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_subtitle_visible_iv_search:
                llSearch.setVisibility(View.VISIBLE);
                break;
            case R.id.toolbar_subtitle_visible_close:
                llSearch.setVisibility(View.GONE);
                break;
            case R.id.toolbar_subtitle_visible_iv_filter:
                presenter.getCountries(1, 100);
                presenter.openDialogFilter(this);
                break;
            case R.id.btn_add_new_contact:
                Intent intent = new Intent(MainListActivity.this,
                        NewPersonActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void getingListContactPerson(ContactPerson contactPerson) {
        list = new ArrayList<>();
        list.addAll(contactPerson.getObjects());
        adapterRecyclerViewContacts = new AdapterRecyclerViewContacts(this, list);
        recyclerView.setAdapter(adapterRecyclerViewContacts);
        adapterRecyclerViewContacts.setOnLoadMoreListener(this);
        recyclerView.setNestedScrollingEnabled(false);

    }

    @Override
    public void getingListContactPersonLoadMore(ContactPerson contactPerson) {
        if (contactPerson.getObjects().size() != 0) {
            list.remove(list.size() - 1);
            adapterRecyclerViewContacts.notifyItemRemoved(list.size());
            list.addAll(contactPerson.getObjects());
            adapterRecyclerViewContacts.setList(list);
            adapterRecyclerViewContacts.notifyDataSetChanged();
        }else{
            sizeNull = true;
            Toast.makeText(this, "Больше нет данных!",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void getCRC() {

    }

    @Override
    public void resiltFilterReady(String country, String region, String city) {
        page=1;
        if (!Objects.equals(country, "") && Objects.equals(region, "") && Objects.equals(city, "")) {
            presenter
            .getListPersonContactsWithFilter(page, size, null, country, null);
        } else if (!Objects.equals(country, "") && !Objects.equals(region, "") && Objects.equals(city, "")) {
            presenter
            .getListPersonContactsWithFilter(page, size, null, country, region);
        } else {
            presenter
            .getListPersonContactsWithFilter(page, size, city, country, region);
        }
    }

    @Override
    public void onLoadMore() {
        adapterRecyclerViewContacts.getList().add(null);
        adapterRecyclerViewContacts.notifyItemInserted(adapterRecyclerViewContacts.getList().size() - 1);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                adapterRecyclerViewContacts
                        .getList()
                        .remove(adapterRecyclerViewContacts.getList().size() - 1);
                adapterRecyclerViewContacts
                        .notifyItemRemoved(adapterRecyclerViewContacts.getList().size());
                page++;
                getNextGsonObject(page, size);
                isLoaded = false;
            }
        }, 1000);
    }

    private void getNextGsonObject(int os, int lim) {
//        gsonList.add(new GsonObjectFilter("responsible.id", "=", String.valueOf(user.getId())));
        presenter.getListPersonContactsLoadMore(os, lim);
    }
}

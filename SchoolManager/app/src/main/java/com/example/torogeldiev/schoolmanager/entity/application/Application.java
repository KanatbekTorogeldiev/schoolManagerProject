package com.example.torogeldiev.schoolmanager.entity.application;

/**
 * Created by Iolana on 21.02.2018.
 */

public class Application {

    private String id;
    private String name;
    private String callBackUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCallBackUrl() {
        return callBackUrl;
    }

    public void setCallBackUrl(String callBackUrl) {
        this.callBackUrl = callBackUrl;
    }
}

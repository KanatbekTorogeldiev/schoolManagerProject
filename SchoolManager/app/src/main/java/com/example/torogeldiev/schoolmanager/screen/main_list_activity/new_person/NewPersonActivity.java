package com.example.torogeldiev.schoolmanager.screen.main_list_activity.new_person;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.torogeldiev.schoolmanager.R;
import com.example.torogeldiev.schoolmanager.entity.contact_person.ContactPerson;
import com.example.torogeldiev.schoolmanager.entity.object.Objec;
import com.example.torogeldiev.schoolmanager.entity.token_user.TokenUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewPersonActivity extends AppCompatActivity implements View.OnClickListener,
        NewPersonActivityView {

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.toolbar_with_title)
    Toolbar toolbar;
    @BindView(R.id.toolbar_with_title_title)
    TextView title;
    @BindView(R.id.toolbar_with_title_subtitle)
    TextView subtitle;
    @BindView(R.id.spinner_country)
    Spinner spnCountry;
    @BindView(R.id.spinner_region)
    Spinner spnRegion;
    @BindView(R.id.spinner_city)
    Spinner spnCity;
    @BindView(R.id.spinner_sex)
    Spinner spnSex;
    @BindView(R.id.edit_surname_new_contact)
    EditText sur_Name;
    @BindView(R.id.edit_name_new_contact)
    EditText _Name;
    @BindView(R.id.edit_second_name_new_contact)
    EditText second_Name;
    @BindView(R.id.spinner_organization)
    Spinner organization;
    @BindView(R.id.edit_phone_new_contact)
    EditText phone;
    @BindView(R.id.edit_email_new_contact)
    EditText e_mail;
    @BindView(R.id.cancel_info_new_contact)
    TextView cancelInfo;
    @BindView(R.id.add_info_new_contact)
    TextView addInfo;
    @BindView(R.id.tv_dob)
    TextView tvDob;
    @BindView(R.id.edit_password_new_contact)
    EditText tvPassword;

    private List<Objec> countries;
    private List<Objec> regions = new ArrayList<>();
    private List<Objec> cities = new ArrayList<>();
    private List<Objec> partners;

    String countryId = "";
    String regionId = "";

    private int cityId;
    String city_Id;
    private String country;
    private String region;
    private String telephone;
    private String email;
    private String surname;
    private String name;
    private String secondName;
    private String sex;
    private String gender;
    private String birthdate;
    private int managerId;
    private int partnerId;
    private String parameter;
    private String additionalContacts;


    int size = 100;
    private NewPersonActivityPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_person);

        ButterKnife.bind(this);

        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        title.setText("Регистрация");
        subtitle.setText("новый контакт");

        presenter = new NewPersonActivityPresenter(this, this);
        presenter.getCountries(1, size);

        addInfo.setOnClickListener(this);
        tvDob.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_info_new_contact:
                progressBar.setVisibility(View.VISIBLE);
                TokenUser token = TokenUser.getToken(this);

                Date date = null;
                SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    telephone = phone.getText().toString();
                    email = e_mail.getText().toString();
                    surname = sur_Name.getText().toString();
                    name = _Name.getText().toString();
                    secondName = second_Name.getText().toString();
                    date = fmt.parse(tvDob.getText().toString());
                    if (Objects.equals(getSex(), "Женский")) sex = "female";
                    else sex = "male";
                    birthdate = tvDob.getText().toString();
                    managerId = Integer.parseInt(token.getUserId());
                    parameter = "";
                    additionalContacts = "";
                    presenter.createPerson(cityId, telephone, email, surname,
                            name, secondName, sex, date, Integer.parseInt(token.getUserId()),
                            tvPassword.getText().toString().trim(),
                            partnerId, parameter, additionalContacts);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                break;
            case R.id.tv_dob:
                presenter.dialogDatePicker();
                break;
        }
    }

    @Override
    public void successCreate(Objec person) {
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, "Пользователь успешно создан!", Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    @Override
    public void getListCountries(ContactPerson person) {
        countries = new ArrayList<>();
        countries.addAll(person.getObjects());
        ArrayAdapter<Objec> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, countries);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnCountry.setAdapter(adapter);

        spnCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition,
                                       long selectedId) {

                Objec objec = countries.get(selectedItemPosition);
                country = objec.getName();
                countryId = String.valueOf(objec.getId());
                presenter.getRegions(countryId, 1, size);
                cities.clear();
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void getListRegions(ContactPerson person) {
        if (person.getObjects().size() != 0) {
            regions.addAll(person.getObjects());
            ArrayAdapter<Objec> adapter = new ArrayAdapter<Objec>(this, android.R.layout.simple_spinner_item, regions);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnRegion.setAdapter(adapter);
            spnRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent,
                                           View itemSelected, int selectedItemPosition, long selectedId) {

                    Objec objec = regions.get(selectedItemPosition);
                    region = objec.getName();
                    regionId = String.valueOf(objec.getId());
                    presenter.getCities(regionId, 1, size);
                    cities.clear();
                }

                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        } else {
            regions.clear();
            cities.clear();
            Toast.makeText(this, "Список регионов пуст", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void getListCities(ContactPerson person) {
        if (person.getObjects().size() != 0) {
            cities.addAll(person.getObjects());
            ArrayAdapter<Objec> adapter =
                    new ArrayAdapter<Objec>(this, android.R.layout.simple_spinner_item, cities);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spnCity.setAdapter(adapter);
            spnCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent,
                                           View itemSelected, int selectedItemPosition, long selectedId) {

                    Objec objec = cities.get(selectedItemPosition);
                    cityId = objec.getId();
                    city_Id = String.valueOf(objec.getId());
                    presenter.getPartners(city_Id, 1, size);

                }

                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        } else {
            cities.clear();
            Toast.makeText(this, "Список городов пуст", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void getListPartners(ContactPerson person) {
        partners = new ArrayList<>();
        partners.addAll(person.getObjects());
        ArrayAdapter<Objec> adapter =
                new ArrayAdapter<>(this
                        , android.R.layout.simple_spinner_item, partners);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        organization.setAdapter(adapter);
        organization.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {

                Objec objec = partners.get(selectedItemPosition);
                partnerId = objec.getId();
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    @Override
    public void choosenDateofB(String date) {
        tvDob.setText(date);
    }

    public String getSex() {
        ArrayAdapter<?> adapter =
                ArrayAdapter.createFromResource(this, R.array.sex, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spnSex.setAdapter(adapter);
        spnSex.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {

                String[] choose = getResources().getStringArray(R.array.sex);
                gender = choose[selectedItemPosition];

            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        return gender;
    }

}

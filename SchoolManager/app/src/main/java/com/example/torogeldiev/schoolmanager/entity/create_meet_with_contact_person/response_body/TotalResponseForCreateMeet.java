package com.example.torogeldiev.schoolmanager.entity.create_meet_with_contact_person.response_body;

import com.example.torogeldiev.schoolmanager.entity.contact_subject.ContactSubjects;
import com.example.torogeldiev.schoolmanager.entity.manager.Manager;
import com.example.torogeldiev.schoolmanager.entity.partner.Partner;

import java.util.List;

/**
 * Created by torogeldiev on 20.02.2018.
 */

public class TotalResponseForCreateMeet {
    private int id;
    private ContactPersonResponse contactPerson;
    private Manager manager;
    private Partner partner;
    private String comment;
    private String finishComment;
    private String createDate;
    private String onDate;
    private String userUpdateDate;
    private int status;
    private String statusName;
    private List<ContactSubjects> contactSubjects;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ContactPersonResponse getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(ContactPersonResponse contactPerson) {
        this.contactPerson = contactPerson;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public Partner getPartner() {
        return partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getFinishComment() {
        return finishComment;
    }

    public void setFinishComment(String finishComment) {
        this.finishComment = finishComment;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getOnDate() {
        return onDate;
    }

    public void setOnDate(String onDate) {
        this.onDate = onDate;
    }

    public String getUserUpdateDate() {
        return userUpdateDate;
    }

    public void setUserUpdateDate(String userUpdateDate) {
        this.userUpdateDate = userUpdateDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public List<ContactSubjects> getContactSubjects() {
        return contactSubjects;
    }

    public void setContactSubjects(List<ContactSubjects> contactSubjects) {
        this.contactSubjects = contactSubjects;
    }
}

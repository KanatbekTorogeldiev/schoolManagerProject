package com.example.torogeldiev.schoolmanager.entity.selected_item_subject_for_adapter;

/**
 * Created by torogeldiev on 21.02.2018.
 */

public class SelectedItemSubjectsForAdapter {
    private int id;
    private String nameSub;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameSub() {
        return nameSub;
    }

    public void setNameSub(String nameSub) {
        this.nameSub = nameSub;
    }
}

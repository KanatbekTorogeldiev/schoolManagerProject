package com.example.torogeldiev.schoolmanager.screen.teacher_blank_meet_or_not_Activity.create_meet_activity;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.torogeldiev.schoolmanager.R;
import com.example.torogeldiev.schoolmanager.adapter.AdapterRecyclerViewSubjects;
import com.example.torogeldiev.schoolmanager.adapter.RecyclerAdapterViewSubjectsChoosen;
import com.example.torogeldiev.schoolmanager.entity.contact_subject.ContactSubjects;
import com.example.torogeldiev.schoolmanager.entity.create_meet_with_contact_person.response_body.TotalResponseForCreateMeet;
import com.example.torogeldiev.schoolmanager.entity.subjects.Subject;
import com.example.torogeldiev.schoolmanager.entity.subjects.response_subjects.ListSubjects;
import com.example.torogeldiev.schoolmanager.entity.token_user.TokenUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateMeetActivity extends AppCompatActivity implements View.OnClickListener, CreateMeetActivityView {

    @BindView(R.id.create_meet_progressBar)
    ProgressBar progressBar;
    @BindView(R.id.title_toolbar)
    TextView titleToolbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_create_meet_choose_date)
    TextView tvDatePiker;
    @BindView(R.id.tv_create_meet_choose_subject)
    TextView tvChooseSubject;
    @BindView(R.id.rv_create_meet_chosen_subject)
    RecyclerView rvChosenSubject;
    @BindView(R.id.tv_create_meet_btn_create_meet)
    TextView btnCreateMeet;
    @BindView(R.id.et_create_meet_comment)
    EditText etComment;

    private CreateMeetActivityPresenter presenter;

    private List<Subject> list = new ArrayList<>();
    private List<Subject> listChosenSubjects = new ArrayList<>();
    private List<Subject> listSelectedSubj = new ArrayList<>();
    private List<ContactSubjects> listContactSubj;

    private Subject subject;

    private int idSelectedSubject;
    private int IdSubject;
    private ContactSubjects contactSubjects;
    private Date date = null;

    //Adapters
    private AdapterRecyclerViewSubjects adapterRecyclerViewSubjects;
    private RecyclerAdapterViewSubjectsChoosen recyclerAdapterViewSubjectsChoosen;

    int idPartner, idContactPer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_meet);
        ButterKnife.bind(this);

        progressBar.setVisibility(View.INVISIBLE);

        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        titleToolbar.setTextColor(getResources().getColor(android.R.color.white));
        titleToolbar.setText("Назначение встречи");

        idContactPer =  getIntent().getIntExtra("contactPersonIdFromTeacherBlank", idContactPer);
        idPartner =  getIntent().getIntExtra("partnerIdFromTeacherBlank", idPartner);

        presenter = new CreateMeetActivityPresenter(this, this);
        presenter.getListSubjects(0, 100);

        tvDatePiker.setOnClickListener(this);
        tvChooseSubject.setOnClickListener(this);
        btnCreateMeet.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_create_meet_choose_date:
                presenter.dialogDatePicker();
                break;
            case R.id.tv_create_meet_choose_subject:
                int selectStatusId = IdSubject;
                dialogRadio(selectStatusId);
                break;
            case R.id.tv_create_meet_btn_create_meet:
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                TokenUser tokenUser = TokenUser.getToken(this);
                int idManager = Integer.parseInt(tokenUser.getUserId());

                String commentary = etComment.getText().toString().trim();
                listContactSubj = new ArrayList<>();
                if (listSelectedSubj.size() != 0 && tvDatePiker.getText() != "" && !Objects.equals(commentary, "")) {
                    progressBar.setVisibility(View.VISIBLE);
                    for (int i = 0; i < listSelectedSubj.size(); i++) {
                        listSelectedSubj.get(i);
                        contactSubjects = new ContactSubjects(etComment.getText().toString().trim(),
                                listSelectedSubj.get(i).getStatus(), listSelectedSubj.get(i).getId());
                        listContactSubj.add(contactSubjects);
                    }
                    try {
                        date = format.parse(tvDatePiker.getText().toString());
                        presenter.createNewMeet(idContactPer, idManager, idPartner, commentary, date,
                                listContactSubj);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                }else{
                    Toast.makeText(this, "Недостаточно введенных данных о встрече",
                            Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void getDate(String date) {
        tvDatePiker.setText(date.trim());
    }

    @Override
    public void getListSubjects(ListSubjects subjects) {
        list.clear();
        list.addAll(subjects.getObjects());
    }

    @Override
    public void removeItem(int position) {
        listSelectedSubj.remove(position);
        recyclerAdapterViewSubjectsChoosen.notifyDataSetChanged();
        if (listSelectedSubj.size() == 0) {
            listSelectedSubj.clear();
        }
    }

    @Override
    public void successCreate(TotalResponseForCreateMeet responseForCreateMeet) {
        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, "Встреча успешно назначена",
                Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    void dialogRadio(int idSelected) {
        if (list.size() == 0) {
            Toast.makeText(this, "Предметы встреч отсутствуют!", Toast.LENGTH_SHORT).show();
        } else {
            View dialogView = View.inflate(this, R.layout.list_meet_subjects, null);
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setView(dialogView);
            final AlertDialog alertDialog = builder.create();
            alertDialog.show();
            RecyclerView mRecyclerView = dialogView.findViewById(R.id.rv_list_subjects);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLayoutManager);
            adapterRecyclerViewSubjects = new AdapterRecyclerViewSubjects(this, list);
            adapterRecyclerViewSubjects.setIdSelect(idSelected);

            listChosenSubjects = adapterRecyclerViewSubjects.getListChosenSubjects();
            mRecyclerView.setAdapter(adapterRecyclerViewSubjects);
            dialogView.findViewById(R.id.tv_add_all_meet_subjects).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    subject = adapterRecyclerViewSubjects.getSubject();
                    if (subject != null) {
                        //listSelectedSubj.add(new Subject(subject.getId(), subject.getName()));
                        listSelectedSubj.addAll(listChosenSubjects);
                        LinearLayoutManager linearLayoutManager =
                                new LinearLayoutManager(CreateMeetActivity.this,
                                        LinearLayoutManager.VERTICAL, false);
                        rvChosenSubject.setLayoutManager(linearLayoutManager);
                        List<Subject> deDupStringList =
                                new ArrayList<>(new HashSet<>(listSelectedSubj));
                        listSelectedSubj.clear();
                        listSelectedSubj.addAll(deDupStringList);
                        recyclerAdapterViewSubjectsChoosen =
                                new RecyclerAdapterViewSubjectsChoosen<>(CreateMeetActivity.this,
                                        listSelectedSubj);
                        rvChosenSubject.setAdapter(recyclerAdapterViewSubjectsChoosen);
                        recyclerAdapterViewSubjectsChoosen.setActivityView(CreateMeetActivity.this);
                    } else {
                        Toast.makeText(CreateMeetActivity.this, "Выберите предмет встречи", Toast.LENGTH_SHORT).show();
                    }
                    alertDialog.dismiss();
                }
            });

            dialogView.findViewById(R.id.tv_cancel_meet_subjects).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
        }
    }

    /*void dialogRadio(int idSelected) {

        if (list.size() == 0) {
            Toast.makeText(this, "Предметы встреч отсутствуют!", Toast.LENGTH_SHORT).show();
        } else {
            View dialogView = View.inflate(this, R.layout.list_meet_subjects, null);
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
            builder.setCancelable(true);
            builder.setView(dialogView);
            final AlertDialog alertDialog = builder.create();
            alertDialog.show();

            RecyclerView mRecyclerView = dialogView.findViewById(R.id.rv_list_subjects);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLayoutManager);
            adapterRecyclerViewSubjects = new AdapterRecyclerViewSubjects(this, list);
            adapterRecyclerViewSubjects.setIdSelect(idSelected);
            mRecyclerView.setAdapter(adapterRecyclerViewSubjects);
            dialogView.findViewById(R.id.tv_add_all_meet_subjects).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    subject = adapterRecyclerViewSubjects.getSubject();
                    if (subject != null) {
                        listSelectedSubj.add(new Subject(subject.getId(), subject.getName()));
                        LinearLayoutManager linearLayoutManager =
                                new LinearLayoutManager(CreateMeetActivity.this,
                                        LinearLayoutManager.VERTICAL, false);
                        rvChosenSubject.setLayoutManager(linearLayoutManager);
                        recyclerAdapterViewSubjectsChoosen =
                                new RecyclerAdapterViewSubjectsChoosen<>(CreateMeetActivity.this,
                                        listSelectedSubj);
                        rvChosenSubject.setAdapter(recyclerAdapterViewSubjectsChoosen);
                        recyclerAdapterViewSubjectsChoosen.setActivityView(CreateMeetActivity.this);
                        recyclerAdapterViewSubjectsChoosen.notifyDataSetChanged();
                    } else {
                        Toast.makeText(CreateMeetActivity.this, "Выберите тип торговой точки", Toast.LENGTH_SHORT).show();
                    }
                    alertDialog.dismiss();
                }
            });

            dialogView.findViewById(R.id.tv_cancel_meet_subjects).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });
        }
    }*/
}

package com.example.torogeldiev.schoolmanager.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.torogeldiev.schoolmanager.R;
import com.example.torogeldiev.schoolmanager.adapter.holder.ChoosenSubjectViewHolder;
import com.example.torogeldiev.schoolmanager.entity.subjects.Subject;
import com.example.torogeldiev.schoolmanager.screen.teacher_blank_meet_or_not_Activity.create_meet_activity.CreateMeetActivityView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by torogeldiev on 21.02.2018.
 */

public class RecyclerAdapterViewSubjectsChoosen<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<T> list = new ArrayList<>();
    private CreateMeetActivityView activityView;
    public RecyclerAdapterViewSubjectsChoosen(Context context, List<T> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_added_subject, parent, false);
        return new ChoosenSubjectViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ChoosenSubjectViewHolder choosenSubjectViewHolder = (ChoosenSubjectViewHolder) holder;
        Subject subject = (Subject) list.get(position);
        choosenSubjectViewHolder.nameSubject.setText(subject.getName() != null ? subject.getName() : "-");
        choosenSubjectViewHolder.idSubject.setText(String.valueOf(subject.getId()));

        choosenSubjectViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activityView != null) {
                    activityView.removeItem(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void checkRepeats(){
        List<T> deDupStringList = new ArrayList<>(new HashSet<>(list));
        this.list = deDupStringList;
    }

    public CreateMeetActivityView getActivityView() {
        return activityView;
    }

    public void setActivityView(CreateMeetActivityView activityView) {
        this.activityView = activityView;
    }
}

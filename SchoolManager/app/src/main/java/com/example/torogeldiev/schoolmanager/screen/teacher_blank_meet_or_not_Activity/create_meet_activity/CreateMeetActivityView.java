package com.example.torogeldiev.schoolmanager.screen.teacher_blank_meet_or_not_Activity.create_meet_activity;

import com.example.torogeldiev.schoolmanager.entity.create_meet_with_contact_person.response_body.TotalResponseForCreateMeet;
import com.example.torogeldiev.schoolmanager.entity.subjects.response_subjects.ListSubjects;

/**
 * Created by torogeldiev on 22.02.2018.
 */

public interface CreateMeetActivityView {
    void getDate(String date);
    void getListSubjects(ListSubjects subjects);
    void removeItem(int position);
    void successCreate(TotalResponseForCreateMeet responseForCreateMeet);
}

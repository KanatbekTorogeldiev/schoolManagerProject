package com.example.torogeldiev.schoolmanager;

import android.app.Application;
import android.util.Log;

import com.example.torogeldiev.schoolmanager.api.UserApi;
import com.example.torogeldiev.schoolmanager.api.UserLogined;
import com.example.torogeldiev.schoolmanager.entity.token_user.TokenUser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by torogeldiev on 14.02.2018.
 */

public class App extends Application {
    private static Retrofit retrofitLogin, retrofit;
    private static UserApi userApi;
    private static UserLogined userLogined;

    @Override
    public void onCreate() {
        super.onCreate();

        OkHttpClient header = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        TokenUser token = TokenUser.getToken(getApplicationContext());
                        Log.d("TOKEN", token.getAccess_token());
                        Log.d("TOKEN", token.getUserId());
                        Request newRequest = chain.request().newBuilder()
                                .addHeader("Authorization", "Bearer " + TokenUser.getToken(getApplicationContext()).getAccess_token())
                                .build();
                        return chain.proceed(newRequest);
                    }
                }).build();

//        OkHttpClient client = header.connectTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).build();
//        OkHttpClient clientToken = new OkHttpClient.Builder()
//                .connectTimeout(60, TimeUnit.SECONDS)
//                .readTimeout(60, TimeUnit.SECONDS)
//                .build();
//        Анвар можно уточнить? Для добавления нового лица(училику, препода итд..) я использую эту функцию?
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();

        retrofitLogin = new Retrofit.Builder()
                .baseUrl("http://apiss.bilimkana.kg/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl("http://apiss.bilimkana.kg/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(header)
                .build();

        userApi = retrofitLogin.create(UserApi.class);
        userLogined = retrofit.create(UserLogined.class);

    }

    public static UserApi getUserApi() {
        return userApi;
    }

    public static UserLogined getUserLogined() {
        return userLogined;
    }
}

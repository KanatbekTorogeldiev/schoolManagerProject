package com.example.torogeldiev.schoolmanager.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.torogeldiev.schoolmanager.R;
import com.example.torogeldiev.schoolmanager.adapter.holder.ContactPersonViewHolder;
import com.example.torogeldiev.schoolmanager.adapter.holder.ProgressBarViewHolder;
import com.example.torogeldiev.schoolmanager.adapter.holder.TeacherBlankListViewHolder;
import com.example.torogeldiev.schoolmanager.api.OnLoadMoreListener;
import com.example.torogeldiev.schoolmanager.entity.contact_subject.ContactSubjects;
import com.example.torogeldiev.schoolmanager.entity.object.Objec;
import com.example.torogeldiev.schoolmanager.entity.parcel.SubjectInfo;
import com.example.torogeldiev.schoolmanager.screen.map.MapActivity;
import com.example.torogeldiev.schoolmanager.screen.teacher_blank_meet_or_not_Activity.finished_meet_activity.FinishedMeetActivity;
import com.example.torogeldiev.schoolmanager.screen.teacher_blank_meet_or_not_Activity.planned_meet_activity.PlannedMeetActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by torogeldiev on 24.02.2018.
 */

public class RecyclerAdapterViewContactMeet extends
        RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Objec> list;
    ArrayList<SubjectInfo> subjects = new ArrayList<>();

    private static final int VIEW_TYPE_LOADING = 1;
    private static final int VIEW_TYPE_ITEM = 0;
    private OnLoadMoreListener onLoadMoreListener;

    public RecyclerAdapterViewContactMeet(Context context, List<Objec> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_teacher_blank, parent, false);
            return new TeacherBlankListViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(context)
                    .inflate(R.layout.item_progressbar, parent, false);
            return new ProgressBarViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TeacherBlankListViewHolder)
        {
            TeacherBlankListViewHolder teacherBlankListViewHolder =
                    (TeacherBlankListViewHolder) holder;
            final Objec objec = list.get(position);

            final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            if(objec!=null) {
                if (objec.getOnDate() != null) {
                    teacherBlankListViewHolder.dateVisit.setText(dateFormat.format(objec.getOnDate()));
                } else {
                    teacherBlankListViewHolder.dateVisit.setText("-");
                }

                if (Objects.equals(objec.getStatus(), "2")) {
                    teacherBlankListViewHolder.showInMap.setVisibility(View.VISIBLE);
                } else {
                    teacherBlankListViewHolder.showInMap.setVisibility(View.GONE);
                }
                teacherBlankListViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Objects.equals(objec.getStatus(), "1")) {
                            Intent intent = new Intent(context, PlannedMeetActivity.class);
                            intent.putExtra("contactId", objec.getId());
                            intent.putExtra("nameContactPerson", objec.getContactPerson().getFullName());
                            intent.putExtra("date", dateFormat.format(objec.getOnDate()));
                            intent.putExtra("nameOrganization", objec.getPartner().getName());
                            intent.putExtra("nameCity", objec.getPartner().getCity().getName());
                            intent.putExtra("comment", objec.getComment());
                            int i = 0;
                            for (ContactSubjects contactSubjects : objec.getContactSubjects()
                                    ) {
                                subjects.add(new SubjectInfo(contactSubjects.getSubject().getName(),
                                        String.valueOf(contactSubjects.getSubject().getStatus()),
                                        contactSubjects.getSubject().getId(),
                                        contactSubjects.getId()));
                                i++;
                            }
                            intent.putParcelableArrayListExtra("subjects", subjects);
                            // context.startActivity(intent);
                            ((Activity) context).startActivityForResult(intent, 1000);
                        } else {
                            Intent intent = new Intent(context, FinishedMeetActivity.class);
                            intent.putExtra("nameContactPerson", objec.getContactPerson().getFullName());
                            intent.putExtra("date", dateFormat.format(objec.getOnDate()));
                            intent.putExtra("nameOrganization", objec.getPartner().getName());
                            intent.putExtra("nameCity", objec.getPartner().getCity().getName());
                            intent.putExtra("comment", objec.getFinishComment());
                            int i = 0;
                            for (ContactSubjects contactSubjects : objec.getContactSubjects()
                                    ) {
                                subjects.add(new SubjectInfo(contactSubjects.getSubject().getName(),
                                        String.valueOf(contactSubjects.getSubject().getStatus()),
                                        contactSubjects.getSubject().getId(),
                                        contactSubjects.getId()));
                                i++;
                            }
                            intent.putParcelableArrayListExtra("subjects", subjects);
                            ((Activity) context).startActivityForResult(intent, 1000);
                            //context.startActivity(intent);
                        }
                    }
                });

                teacherBlankListViewHolder.showInMap.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, MapActivity.class);
                        intent.putExtra("nameOrganization", objec.getPartner().getName());
                        intent.putExtra("city", objec.getPartner().getCity().getName());
                        intent.putExtra("longitude", objec.getPartner().getLongitude());
                        intent.putExtra("latitude", objec.getPartner().getLatitude());
                        context.startActivity(intent);
                    }
                });
            }
        }else if (holder instanceof ProgressBarViewHolder)
        {
            ProgressBarViewHolder progressBarViewHolder = (ProgressBarViewHolder) holder;
            progressBarViewHolder.progressBar.setIndeterminate(true);
        }
    }

    public OnLoadMoreListener getOnLoadMoreListener() {
        return onLoadMoreListener;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public List<Objec> getList() {
        return list;
    }

    public void setList(List<Objec> list) {
        this.list = list;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

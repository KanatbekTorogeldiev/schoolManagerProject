package com.example.torogeldiev.schoolmanager.screen.login;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.example.torogeldiev.schoolmanager.App;
import com.example.torogeldiev.schoolmanager.entity.token_user.TokenUser;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by torogeldiev on 15.02.2018.
 */

public class LoginActivityPresenter {
    private Context context;
    private LoginActivityView activityView;

    public LoginActivityPresenter(Context context, LoginActivityView activityView) {
        this.context = context;
        this.activityView = activityView;
    }

    void setLogin(String login, String password){
        App.getUserApi().authToken("as13_4jrW48)3j",password,login, "password")
                .enqueue(new Callback<TokenUser>() {
                    @Override
                    public void onResponse(Call<TokenUser> call, Response<TokenUser> response) {
                        if(response.isSuccessful()){
                            Log.d("TOKEN", response.body().getAccess_token());
                            activityView.getTokenAuth(response.body());
                        }else{
                            Toast.makeText(context, "Не верный логин/пароль", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<TokenUser> call, Throwable t) {
                        Toast.makeText(context, "Не удалось соединиться с сервером", Toast.LENGTH_SHORT).show();
                    }
                });

    }

}

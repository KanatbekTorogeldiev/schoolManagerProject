package com.example.torogeldiev.schoolmanager.entity.token_user;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * Created by torogeldiev on 14.02.2018.
 */


public class TokenUser {
    private String access_token;
    private String token_type;
    private int expires_in;
    private String userName;
    private String profileId;
    private String role;
    private String userId;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public static void saveToken(Context context, TokenUser token)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokenUser",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new GsonBuilder().create();
        String tokenUser = gson.toJson(token);
        editor.putString("token",tokenUser);
        editor.apply();
    }

    public static TokenUser getToken(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokenUser",Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("token","");
        Gson gson = new GsonBuilder().create();
        TokenUser tokenUser = gson.fromJson(token, new TypeToken<TokenUser>() {
        }.getType());
        return tokenUser;
    }

    public static void deletToken(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("tokenUser",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("token");
        editor.apply();
    }

}

package com.example.torogeldiev.schoolmanager.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.torogeldiev.schoolmanager.R;
import com.example.torogeldiev.schoolmanager.adapter.holder.SubjectViewHolder;
import com.example.torogeldiev.schoolmanager.entity.contact_subject.ContactSubjects;
import com.example.torogeldiev.schoolmanager.entity.subjects.Subject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Iolana on 21.02.2018.
 */
public class AdapterRecyclerViewSubjects extends
        RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Subject> list;
    private Subject subject;
    private int selectId = -1;
    private int idSelect;

    private List<Subject> listChosenSubjects = new ArrayList<>();

    public AdapterRecyclerViewSubjects(Context context, List<Subject> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_subject,
                parent, false);
        return new SubjectViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final SubjectViewHolder subjectViewHolder = (SubjectViewHolder) holder;
        //subjectViewHolder.checkSubject.setEnabled(false);
        subjectViewHolder.subject.setText(list.get(position).getName());
        subjectViewHolder.checkSubject.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                subject = list.get(position);
                if(subjectViewHolder.checkSubject.isChecked()){
                    listChosenSubjects.add(subject);
                    selectId = list.get(position).getId();
                    subjectViewHolder.checkSubject.setChecked(true);
                    notifyDataSetChanged();
                }
                else{
                    subjectViewHolder.checkSubject.setChecked(false);
                    listChosenSubjects.remove(subject);
                }
            }
        });
        subjectViewHolder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subject = list.get(position);
                if(!subjectViewHolder.checkSubject.isChecked()) {
                    listChosenSubjects.add(subject);
                    selectId = list.get(position).getId();
                    subjectViewHolder.checkSubject.setChecked(true);
                    notifyDataSetChanged();
                }else{
                    subjectViewHolder.checkSubject.setChecked(false);
                    listChosenSubjects.remove(subject);
                }

            }
        });

       /* if (selectId==list.get(position).getId()) {
            subjectViewHolder.checkSubject.setChecked(true);
        }
        else {
            subjectViewHolder.checkSubject.setChecked(false);
        }

        if (idSelect == list.get(position).getId() && selectId == -1) {
            subjectViewHolder.checkSubject.setChecked(true);
        }*/
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public List<Subject> getListChosenSubjects() {
        return listChosenSubjects;
    }

    public int getIdSelect() {
        return idSelect;
    }

    public void setIdSelect(int idSelect) {
        this.idSelect = idSelect;
    }

    public Subject getSubject() {
        return subject;
    }

}
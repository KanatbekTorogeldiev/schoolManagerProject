package com.example.torogeldiev.schoolmanager.screen.main_list_activity.new_person;

import com.example.torogeldiev.schoolmanager.entity.contact_person.ContactPerson;
import com.example.torogeldiev.schoolmanager.entity.object.Objec;

/**
 * Created by torogeldiev on 16.02.2018.
 */

public interface NewPersonActivityView {

    void successCreate(Objec objec);
    void getListCountries(ContactPerson person);
    void getListRegions(ContactPerson person);
    void getListCities(ContactPerson person);
    void getListPartners(ContactPerson person);
    void choosenDateofB(String date);
}


package com.example.torogeldiev.schoolmanager.screen.login;

import com.example.torogeldiev.schoolmanager.entity.token_user.TokenUser;

/**
 * Created by torogeldiev on 15.02.2018.
 */

public interface LoginActivityView {
    void getTokenAuth(TokenUser tokenUser);
}

package com.example.torogeldiev.schoolmanager.entity.login_in_body;

/**
 * Created by torogeldiev on 15.02.2018.
 */

public class LogInBody {
    private String password;
    private String username;
    private String grant_type;

    public LogInBody(String password, String username, String grant_type) {
        this.password = password;
        this.username = username;
        this.grant_type = grant_type;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGrant_type() {
        return grant_type;
    }

    public void setGrant_type(String grant_type) {
        this.grant_type = grant_type;
    }
}

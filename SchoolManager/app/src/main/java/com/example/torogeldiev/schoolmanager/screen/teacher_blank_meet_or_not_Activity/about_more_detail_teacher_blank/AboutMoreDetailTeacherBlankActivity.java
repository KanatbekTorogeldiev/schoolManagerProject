package com.example.torogeldiev.schoolmanager.screen.teacher_blank_meet_or_not_Activity.about_more_detail_teacher_blank;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.torogeldiev.schoolmanager.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutMoreDetailTeacherBlankActivity extends AppCompatActivity {

    @BindView(R.id.meeting_subject)
    TextView textMeetingSubject;
    @BindView(R.id.comment_of_subject)
    EditText editCommentSubject;
    @BindView(R.id.btn_send_comment)
    Button buttonSendComment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_more_detail_teacher_blank);

        ButterKnife.bind(this);
    }
}

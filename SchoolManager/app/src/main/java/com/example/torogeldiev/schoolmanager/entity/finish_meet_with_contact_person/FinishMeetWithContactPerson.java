package com.example.torogeldiev.schoolmanager.entity.finish_meet_with_contact_person;

import com.example.torogeldiev.schoolmanager.entity.contact_subject.ContactSubjects;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by torogeldiev on 20.02.2018.
 */

public class FinishMeetWithContactPerson {
    private int id;
    private String comment;
    private ArrayList<ContactSubjects> contactSubjects;

    public FinishMeetWithContactPerson(int id, String comment, ArrayList<ContactSubjects> contactSubjects) {
        this.id = id;
        this.comment = comment;
        this.contactSubjects = contactSubjects;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<ContactSubjects> getContactSubjects() {
        return contactSubjects;
    }

    public void setContactSubjects(ArrayList<ContactSubjects> contactSubjects) {
        this.contactSubjects = contactSubjects;
    }
}

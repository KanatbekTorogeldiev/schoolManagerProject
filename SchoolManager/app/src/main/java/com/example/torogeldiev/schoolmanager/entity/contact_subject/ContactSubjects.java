package com.example.torogeldiev.schoolmanager.entity.contact_subject;

import com.example.torogeldiev.schoolmanager.entity.subjects.Subject;

/**
 * Created by torogeldiev on 20.02.2018.
 */

public class ContactSubjects {

    private int id;
    private String comment;
    private int status;
    private int subjectId;
    private Subject subject;
    private String statusName;
    private String finishComment;
    private String createDate;

    public ContactSubjects(String comment, int  status, int subjectId) {
        this.comment = comment;
        this.status = status;
        this.subjectId = subjectId;
    }

    public ContactSubjects(String comment, int  status, int subjectId, int id) {
        this.comment = comment;
        this.status = status;
        this.subjectId = subjectId;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getFinishComment() {
        return finishComment;
    }

    public void setFinishComment(String finishComment) {
        this.finishComment = finishComment;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

}

package com.example.torogeldiev.schoolmanager.screen.teacher_blank_meet_or_not_Activity;

import android.content.Context;
import android.widget.Toast;

import com.example.torogeldiev.schoolmanager.App;
import com.example.torogeldiev.schoolmanager.entity.contact_person.ContactPerson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by torogeldiev on 15.02.2018.
 */

public class TeacherBlankMeetOrNotActivityPresenter {

    private Context context;
    private TeacherBlankMeetOrNotActivityView teacherBlankMeetOrNotActivityView;

    public TeacherBlankMeetOrNotActivityPresenter(Context context, TeacherBlankMeetOrNotActivityView teacherBlankMeetOrNotActivityView) {
        this.context = context;
        this.teacherBlankMeetOrNotActivityView = teacherBlankMeetOrNotActivityView;
    }

    void getListPlannedContacts(String contactPerson, String status, int page, int size){
        App.getUserLogined().getListContacts(contactPerson, status, page, size).enqueue(new Callback<ContactPerson>() {
            @Override
            public void onResponse(Call<ContactPerson> call, Response<ContactPerson> response) {
                if(response.isSuccessful()){
                    teacherBlankMeetOrNotActivityView.getListPlannedContacts(response.body());
                }
                else{
                    Toast.makeText(context, "Ошибка", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ContactPerson> call, Throwable t) {
                Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void getListPlannedContactsLoadMore(String contactPerson, String status, int page, int size){
        App.getUserLogined().getListContacts(contactPerson, status, page, size).enqueue(new Callback<ContactPerson>() {
            @Override
            public void onResponse(Call<ContactPerson> call, Response<ContactPerson> response) {
                if(response.isSuccessful()){
                    teacherBlankMeetOrNotActivityView
                            .getingListPlannedContactsLoadMore(response.body());
                }
                else{
                    Toast.makeText(context, "Ошибка", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ContactPerson> call, Throwable t) {
                Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }
}




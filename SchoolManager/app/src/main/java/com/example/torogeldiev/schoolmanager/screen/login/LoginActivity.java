package com.example.torogeldiev.schoolmanager.screen.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.example.torogeldiev.schoolmanager.R;
import com.example.torogeldiev.schoolmanager.entity.token_user.TokenUser;
import com.example.torogeldiev.schoolmanager.entity.login_in_body.LogInBody;
import com.example.torogeldiev.schoolmanager.screen.main_list_activity.MainListActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements LoginActivityView, View.OnClickListener {

    @BindView(R.id.login_progressBar)
    ProgressBar progressBar;
    @BindView(R.id.loginbtn)
    TextView btnLogin;
    @BindView(R.id.et_user_login)
    EditText etLogin;
    @BindView(R.id.et_user_password)
    EditText etPassword;

    private LoginActivityPresenter presenter;
    SharedPreferences sPref;
    final String APP_PREF ="app_pref";
    final String LOGIN = "login";
    final String PASSWORD = "password";

    String login_;
    String password_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        presenter = new LoginActivityPresenter(this, this);
        sPref = getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        if(sPref.contains(LOGIN) && sPref.contains(PASSWORD)){
            progressBar.setVisibility(View.VISIBLE);
            login_=sPref.getString(LOGIN, "");
            password_=sPref.getString(PASSWORD, "");
            etLogin.setText(login_);
            etPassword.setText(password_);
            presenter.setLogin(login_,password_);
        }


        btnLogin.setOnClickListener(this);
    }

    @Override
    public void getTokenAuth(TokenUser tokenUser) {
        if(tokenUser != null){
            progressBar.setVisibility(View.INVISIBLE);
            TokenUser.saveToken(this, tokenUser);
            Intent intent = new Intent(LoginActivity.this, MainListActivity.class);
            intent.putExtra("tokenUser",  tokenUser.getUserId());
            startActivity(intent);
        }else{
            progressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginbtn:
                progressBar.setVisibility(View.VISIBLE);
                String login = etLogin.getText().toString().trim();
                String password = etPassword.getText().toString().trim();
                if(!(sPref.contains(LOGIN) && sPref.contains(PASSWORD))) {
                    SharedPreferences.Editor ed = sPref.edit();
                    ed.putString(LOGIN, login);
                    ed.putString(PASSWORD, password);
                    ed.apply();
                }
                LogInBody logInBody = new LogInBody(password, login, "password");
                presenter.setLogin(login,password);
                break;
        }
    }
}

package com.example.torogeldiev.schoolmanager.screen.teacher_blank_meet_or_not_Activity;

import com.example.torogeldiev.schoolmanager.entity.contact_person.ContactPerson;

/**
 * Created by torogeldiev on 15.02.2018.
 */

public interface TeacherBlankMeetOrNotActivityView {

    void getListPlannedContacts(ContactPerson person);
    void getingListPlannedContactsLoadMore(ContactPerson contactPerson);
    void getCRC();
}

package com.example.torogeldiev.schoolmanager.entity.contact_person;

import com.example.torogeldiev.schoolmanager.entity.object.Objec;

import java.util.List;

/**
 * Created by torogeldiev on 15.02.2018.
 */

public class ContactPerson {
    private int count;
    private int page;
    private int size;
    private List<Objec> objects;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<Objec> getObjects() {
        return objects;
    }

    public void setObjects(List<Objec> objects) {
        this.objects = objects;
    }
}

package com.example.torogeldiev.schoolmanager.screen.map;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.torogeldiev.schoolmanager.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapActivity extends AppCompatActivity
        implements OnMapReadyCallback {

    @BindView(R.id.toolbar_with_title)
    Toolbar toolbar;
    @BindView(R.id.toolbar_with_title_title)
    TextView tv_title;
    @BindView(R.id.toolbar_with_title_subtitle)
    TextView tv_subtitle;

    String nameOrganization;
    String nameCity;
    double lng;
    double lat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);

        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        nameOrganization = getIntent().getStringExtra("nameOrganization");
        tv_title.setText(nameOrganization);
        nameCity = getIntent().getStringExtra("city");
        tv_subtitle.setText(nameCity);

        lng = Double.parseDouble(getIntent().getStringExtra("longitude"));
        lat =  Double.parseDouble(getIntent().getStringExtra("latitude"));

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        LatLng test = new LatLng(lat, lng);
        googleMap.addMarker(new MarkerOptions()
                .position(test)
                .title(nameOrganization));
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        try {
            googleMap.setMyLocationEnabled(true);
        }catch(SecurityException e){
            e.printStackTrace();
        }
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(test)
                .zoom(15)
                .bearing(0)
                .tilt(0)
                .build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        googleMap.moveCamera(cameraUpdate);
        //googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}


package com.example.torogeldiev.schoolmanager.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.torogeldiev.schoolmanager.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by torogeldiev on 21.02.2018.
 */

public class ChoosenSubjectViewHolder extends RecyclerView.ViewHolder{

    @BindView(R.id.tv_list_item_added_subject_id_subject)
    public TextView idSubject;
    @BindView(R.id.added_subject)
    public TextView nameSubject;
    @BindView(R.id.tv_list_item_added_subject_delete)
    public ImageView delete;

    public ChoosenSubjectViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
}

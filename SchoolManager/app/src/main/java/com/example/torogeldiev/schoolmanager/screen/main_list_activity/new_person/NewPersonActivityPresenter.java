package com.example.torogeldiev.schoolmanager.screen.main_list_activity.new_person;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.torogeldiev.schoolmanager.App;
import com.example.torogeldiev.schoolmanager.R;
import com.example.torogeldiev.schoolmanager.entity.contact_person.ContactPerson;
import com.example.torogeldiev.schoolmanager.entity.contact_person.CreateContatcPerson;
import com.example.torogeldiev.schoolmanager.entity.object.Objec;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by torogeldiev on 16.02.2018.
 */

public class NewPersonActivityPresenter implements View.OnClickListener {
    private Context context;
    private NewPersonActivityView activityView;

    private TextView tvBtnDatePickerReady, tvBtnDatePickerCancel;
    private android.support.v7.app.AlertDialog alertDialogDatePicker;
    private DatePicker datePicker;
    private String datesForSaving = "";

    public NewPersonActivityPresenter(Context context, NewPersonActivityView activityView) {
        this.context = context;
        this.activityView = activityView;
    }

    void createPerson(int cityId, String telephone, String email, String surname, String name, String secondName, String sex, Date birthdate, int managerId,String password, int partnerId, String parameter, String additionalContacts) {
        CreateContatcPerson person = new CreateContatcPerson(cityId, telephone, email, surname, name, secondName, sex, birthdate, managerId, password, partnerId, parameter, additionalContacts);
        App.getUserLogined().createContactPerson(person).enqueue(new Callback<Objec>() {
            @Override
            public void onResponse(Call<Objec> call, Response<Objec> response) {
                if (response.isSuccessful()) {
                    activityView.successCreate(response.body());
                } else {
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Objec> call, Throwable t) {
                Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void getCountries(int page, int size) {
        App.getUserLogined().getCountries(page, size).enqueue(new Callback<ContactPerson>() {
            @Override
            public void onResponse(Call<ContactPerson> call, Response<ContactPerson> response) {
                if (response.isSuccessful()) {
                    activityView.getListCountries(response.body());
                } else {
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ContactPerson> call, Throwable t) {
                Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void getRegions(String id, int page, int size) {
        App.getUserLogined().getRegions(page, size, id).enqueue(new Callback<ContactPerson>() {
            @Override
            public void onResponse(Call<ContactPerson> call, Response<ContactPerson> response) {
                if (response.isSuccessful()) {
                    activityView.getListRegions(response.body());
                } else {
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ContactPerson> call, Throwable t) {
                Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void getCities(String id, int page, int size) {
        App.getUserLogined().getCities(page, size, id).enqueue(new Callback<ContactPerson>() {
            @Override
            public void onResponse(Call<ContactPerson> call, Response<ContactPerson> response) {
                if (response.isSuccessful()) {
                    activityView.getListCities(response.body());
                } else {
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ContactPerson> call, Throwable t) {
                Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });

    }

    void getPartners(String cityId, int page, int size){
        App.getUserLogined().getPartners(page, size, cityId).enqueue(new Callback<ContactPerson>() {
            @Override
            public void onResponse(Call<ContactPerson> call, Response<ContactPerson> response) {
                if (response.isSuccessful()) {
                    activityView.getListPartners(response.body());
                } else {
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ContactPerson> call, Throwable t) {
                Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Диалоговое окно для выбора даты
     */
    public void dialogDatePicker() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Activity activity = (Activity) context;
        assert inflater != null;
        View view = inflater.inflate(R.layout.dialog_datepicker, (ViewGroup) activity.findViewById(R.id.dialog_date_picker));
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setView(view);
        initFromDialogDatePick(view);
        tvBtnDatePickerReady.setOnClickListener(this);
        tvBtnDatePickerCancel.setOnClickListener(this);
        alertDialogDatePicker = builder.create();
        alertDialogDatePicker.show();
    }

    /**
     * Инициализация элементов из диалога даты dialog_datepicker
     */
    public void initFromDialogDatePick(View view) {
        datePicker = view.findViewById(R.id.datePicker);
        tvBtnDatePickerReady = view.findViewById(R.id.tv_date_picker_ready);
        tvBtnDatePickerCancel = view.findViewById(R.id.tv_date_picker_cancel);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_date_picker_ready:
                int year = datePicker.getYear();
                int month = datePicker.getMonth() + 1;
                int day = datePicker.getDayOfMonth();
                if (day < 10 && month < 10) {
                    datesForSaving = year + "-0" + month + "-0" + day;
                } else if (day < 10 && month >= 10) {
                    datesForSaving = year + "-" + month + "-0" + day;
                } else if (day >= 10 && month < 10) {
                    datesForSaving = year + "-0" + month + "-" + day;
                } else {
                    datesForSaving = year + "-" + month + "-" + day;
                }
                activityView.choosenDateofB(datesForSaving);
                alertDialogDatePicker.dismiss();
                break;
            case R.id.tv_date_picker_cancel:
                alertDialogDatePicker.dismiss();
                break;
        }
    }
}

package com.example.torogeldiev.schoolmanager.entity.create_meet_with_contact_person;

import com.example.torogeldiev.schoolmanager.entity.contact_subject.ContactSubjects;

import java.util.Date;
import java.util.List;

/**
 * Created by torogeldiev on 20.02.2018.
 */

public class CreateMeetWithContactPerson {

    private int id;
    private int contactPersonId;
    private int managerId;
    private int partnerId;
    private String comment;
    private Date onDate;
    private List<ContactSubjects> contactSubjects;

    public CreateMeetWithContactPerson(int contactPersonId, int managerId,
                                       int partnerId, String comment,
                                       Date onDate, List<ContactSubjects> contactSubjects) {
        this.contactPersonId = contactPersonId;
        this.managerId = managerId;
        this.partnerId = partnerId;
        this.comment = comment;
        this.onDate = onDate;
        this.contactSubjects = contactSubjects;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(int contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    public int getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(int partnerId) {
        this.partnerId = partnerId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getOnDate() {
        return onDate;
    }

    public void setOnDate(Date onDate) {
        this.onDate = onDate;
    }

    public List<ContactSubjects> getContactSubjects() {
        return contactSubjects;
    }

    public void setContactSubjects(List<ContactSubjects> contactSubjects) {
        this.contactSubjects = contactSubjects;
    }
}

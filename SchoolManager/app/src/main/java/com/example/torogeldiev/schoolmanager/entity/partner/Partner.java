package com.example.torogeldiev.schoolmanager.entity.partner;

import com.example.torogeldiev.schoolmanager.entity.city.City;

/**
 * Created by torogeldiev on 15.02.2018.
 */

public class Partner {
    private int id;
    private String mainContactPerson;
    private String code;
    private String name;
    private City city;
    private String longitude;
    private String latitude;
    private int contactPersonsCount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMainContactPerson() {
        return mainContactPerson;
    }

    public void setMainContactPerson(String mainContactPerson) {
        this.mainContactPerson = mainContactPerson;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public int getContactPersonsCount() {
        return contactPersonsCount;
    }

    public void setContactPersonsCount(int contactPersonsCount) {
        this.contactPersonsCount = contactPersonsCount;
    }
}

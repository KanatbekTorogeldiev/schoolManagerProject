package com.example.torogeldiev.schoolmanager.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.torogeldiev.schoolmanager.R;
import com.example.torogeldiev.schoolmanager.adapter.holder.TeacherBlankListViewHolder;
import com.example.torogeldiev.schoolmanager.screen.map.MapActivity;
import com.example.torogeldiev.schoolmanager.screen.teacher_blank_meet_or_not_Activity.about_more_detail_teacher_blank.AboutMoreDetailTeacherBlankActivity;

import java.util.List;

/**
 * Created by torogeldiev on 15.02.2018.
 */

public class AdapterRecyclerViewTeacherBlankList<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<T> list;

    public AdapterRecyclerViewTeacherBlankList(Context context, List<T> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_teacher_blank, parent, false);
        return new TeacherBlankListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        TeacherBlankListViewHolder teacherBlankListViewHolder = (TeacherBlankListViewHolder) holder;

        teacherBlankListViewHolder.dateVisit.setText("");
        teacherBlankListViewHolder.showInMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,MapActivity.class);
                context.startActivity(intent);
            }
        });
        teacherBlankListViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,AboutMoreDetailTeacherBlankActivity.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}

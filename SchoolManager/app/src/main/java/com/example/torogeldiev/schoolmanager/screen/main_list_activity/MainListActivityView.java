package com.example.torogeldiev.schoolmanager.screen.main_list_activity;

import com.example.torogeldiev.schoolmanager.entity.contact_person.ContactPerson;

import java.util.List;

/**
 * Created by torogeldiev on 15.02.2018.
 */

public interface MainListActivityView {
    void getingListContactPerson(ContactPerson contactPerson);
    void getingListContactPersonLoadMore(ContactPerson contactPerson);
    void getCRC();
    void resiltFilterReady(String country, String region, String city);
}

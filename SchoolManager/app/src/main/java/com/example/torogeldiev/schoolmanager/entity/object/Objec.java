package com.example.torogeldiev.schoolmanager.entity.object;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.torogeldiev.schoolmanager.entity.city.City;
import com.example.torogeldiev.schoolmanager.entity.contact_subject.ContactSubjects;
import com.example.torogeldiev.schoolmanager.entity.country.Country;
import com.example.torogeldiev.schoolmanager.entity.manager.Manager;
import com.example.torogeldiev.schoolmanager.entity.partner.Partner;
import com.example.torogeldiev.schoolmanager.entity.region.Region;

import java.util.Date;
import java.util.List;

/**
 * Created by torogeldiev on 15.02.2018.
 */

public class Objec{

    private String mString;
    private int id;
    private String fullName;
    private Partner partner;
    private Country country;
    private Region region;
    private City city;
    private int citizenshipId;
    private String citizenshipName;
    private Date date;
    private String telephone;
    private String email;
    private String surname;
    private String name;
    private String secondName;
    private String sex;
    private Date birthdate;
    private Manager manager;
    private String parameter;
    private String login;
    private String additionalContacts;

    //for getting contacts

    private String comment;
    private String finishComment;
    private String createDate;
    private Date onDate;
    private String userUpdateDate;
    private String status;
    private String statusName;
    private List<ContactSubjects> contactSubjects;

    private Objec contactPerson;

    public String getString() {
        return mString;
    }

    public void setString(String string) {
        mString = string;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getFinishComment() {
        return finishComment;
    }

    public void setFinishComment(String finishComment) {
        this.finishComment = finishComment;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Date getOnDate() {
        return onDate;
    }

    public void setOnDate(Date onDate) {
        this.onDate = onDate;
    }

    public String getUserUpdateDate() {
        return userUpdateDate;
    }

    public void setUserUpdateDate(String userUpdateDate) {
        this.userUpdateDate = userUpdateDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public List<ContactSubjects> getContactSubjects() {
        return contactSubjects;
    }

    public void setContactSubjects(List<ContactSubjects> contactSubjects) {
        this.contactSubjects = contactSubjects;
    }

    public String getmString() {
        return mString;
    }

    public void setmString(String mString) {
        this.mString = mString;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Partner getPartner() {
        return partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public int getCitizenshipId() {
        return citizenshipId;
    }

    public void setCitizenshipId(int citizenshipId) {
        this.citizenshipId = citizenshipId;
    }

    public String getCitizenshipName() {
        return citizenshipName;
    }

    public void setCitizenshipName(String citizenshipName) {
        this.citizenshipName = citizenshipName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAdditionalContacts() {
        return additionalContacts;
    }

    public void setAdditionalContacts(String additionalContacts) {
        this.additionalContacts = additionalContacts;
    }

    public Objec getContactPerson() {
        return contactPerson;
    }

    public String toString(){
        return this.name;
    }
}

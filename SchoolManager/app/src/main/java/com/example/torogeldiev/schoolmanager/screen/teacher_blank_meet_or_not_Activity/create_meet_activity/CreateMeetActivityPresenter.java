package com.example.torogeldiev.schoolmanager.screen.teacher_blank_meet_or_not_Activity.create_meet_activity;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.torogeldiev.schoolmanager.App;
import com.example.torogeldiev.schoolmanager.R;
import com.example.torogeldiev.schoolmanager.adapter.AdapterRecyclerViewSubjects;
import com.example.torogeldiev.schoolmanager.entity.contact_subject.ContactSubjects;
import com.example.torogeldiev.schoolmanager.entity.create_meet_with_contact_person.CreateMeetWithContactPerson;
import com.example.torogeldiev.schoolmanager.entity.create_meet_with_contact_person.response_body.TotalResponseForCreateMeet;
import com.example.torogeldiev.schoolmanager.entity.subjects.Subject;
import com.example.torogeldiev.schoolmanager.entity.subjects.response_subjects.ListSubjects;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by torogeldiev on 22.02.2018.
 */

public class CreateMeetActivityPresenter implements View.OnClickListener {

    private Context context;
    private CreateMeetActivityView createMeetActivityView;

    //listSubjects and Adapter
    private AdapterRecyclerViewSubjects adapterRecyclerViewSubjects;
    public List<Subject> list = new ArrayList<>();


    //Datapikers value
    private TextView tvBtnDatePickerReady, tvBtnDatePickerCancel;
    private android.support.v7.app.AlertDialog alertDialogDatePicker;
    private DatePicker datePicker;
    private String datesForSaving = "";


    public CreateMeetActivityPresenter(Context context, CreateMeetActivityView createMeetActivityView) {
        this.context = context;
        this.createMeetActivityView = createMeetActivityView;
    }

    /**
     * Запрос для создания новой встречи
     */
    void createNewMeet(int contactPersonId, int managerId, int partnerId, String comment, Date onDate, List<ContactSubjects> contactSubjects) {
        CreateMeetWithContactPerson createMeet =
                new CreateMeetWithContactPerson(
                        contactPersonId,
                        managerId,
                        partnerId,
                        comment,
                        onDate,
                        contactSubjects);
        App.getUserLogined().createMeet(createMeet).enqueue(new Callback<TotalResponseForCreateMeet>() {
            @Override
            public void onResponse(Call<TotalResponseForCreateMeet> call, Response<TotalResponseForCreateMeet> response) {
                if (response.isSuccessful()) {
                    createMeetActivityView.successCreate(response.body());

                } else {
                    if(response.code()==400){
                        Toast.makeText(context, "Введенные данные неверны",
                                Toast.LENGTH_SHORT).show();
                    }
                    //Toast.makeText(context, ""+response.errorBody(),
                            //Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TotalResponseForCreateMeet> call, Throwable t) {
                Toast.makeText(context, "Не удалось установить соединение с сервером",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     * Плучение списка предмет встреч
     */
    void getListSubjects(int page, int size) {
        App.getUserLogined().getListSubjects(page, size).enqueue(new Callback<ListSubjects>() {
            @Override
            public void onResponse(Call<ListSubjects> call, Response<ListSubjects> response) {
                if (response.isSuccessful()) {
                    createMeetActivityView.getListSubjects(response.body());
                } else {
                    Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ListSubjects> call, Throwable t) {
                Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
            }
        });
    }

//    void openSubjectList(){
//        Activity activity = (Activity) context;
//        list.addAll(GlobalVar.subjects);
//        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        assert layoutInflater != null;
//        View viewDialog = layoutInflater.inflate(R.layout.list_meet_subjects, (ViewGroup) activity.findViewById(R.id.dialog_add_new_meet_date));
//        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//        builder.setView(viewDialog);
//        builder.setCancelable(true);
//        final AlertDialog alertDialog = builder.create();
//        alertDialog.show();
//        RecyclerView mRecyclerView = viewDialog.findViewById(R.id.rv_list_subjects);
//        mRecyclerView.setHasFixedSize(true);
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
//        mRecyclerView.setLayoutManager(mLayoutManager);
//        adapterRecyclerViewSubjects = new AdapterRecyclerViewSubjects(context, list);
////        adapterRecyclerViewSubjects.setIdSelect(idSelected);
//        mRecyclerView.setAdapter(adapterRecyclerViewSubjects);
//        adapterRecyclerViewSubjects.notifyDataSetChanged();
//        dialogView.findViewById(R.id.tv_add_all_meet_subjects).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                subject = adapterRecyclerViewSubjects.getSubject();
//                if (subject != null) {
//                    /**
//                     * здесь берется название и id выбранного элемента из списка предметов
//                     */
//                    subjectId = subject.getId();
//                    subjectName = subject.getName();
//                    teacherBlankMeetOrNotActivityView.setSelectedDatas(subjectId, subjectName);
//                    dateDialogForm.dismiss();
//                } else {
//                    Toast.makeText(context, "Выберите предмет встречи!", Toast.LENGTH_SHORT).show();
//                }
//
//                alertDialog.dismiss();
//            }
//        });
//
//        viewDialog.findViewById(R.id.tv_cancel_meet_subjects).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialog.dismiss();
//            }
//        });
//
//    }

    /**
     * Диалоговое окно для выбора даты
     */
    public void dialogDatePicker() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Activity activity = (Activity) context;
        assert inflater != null;
        View view = inflater.inflate(R.layout.dialog_datepicker, (ViewGroup) activity.findViewById(R.id.dialog_date_picker));
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setView(view);
        initFromDialogDatePick(view);
        tvBtnDatePickerReady.setOnClickListener(this);
        tvBtnDatePickerCancel.setOnClickListener(this);
        alertDialogDatePicker = builder.create();
        alertDialogDatePicker.show();
    }

    /**
     * Инициализация элементов из диалога даты dialog_datepicker
     */
    public void initFromDialogDatePick(View view) {
        datePicker = view.findViewById(R.id.datePicker);
        tvBtnDatePickerReady = view.findViewById(R.id.tv_date_picker_ready);
        tvBtnDatePickerCancel = view.findViewById(R.id.tv_date_picker_cancel);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_date_picker_ready:
                int year = datePicker.getYear();
                int month = datePicker.getMonth() + 1;
                int day = datePicker.getDayOfMonth();
                if (day < 10 && month < 10) {
                    datesForSaving = year + "-0" + month + "-0" + day;
                } else if (day < 10 && month >= 10) {
                    datesForSaving = year + "-" + month + "-0" + day;
                } else if (day >= 10 && month < 10) {
                    datesForSaving = year + "-0" + month + "-" + day;
                } else {
                    datesForSaving = year + "-" + month + "-" + day;
                }
                createMeetActivityView.getDate(datesForSaving);
                alertDialogDatePicker.dismiss();
                break;
            case R.id.tv_date_picker_cancel:
                alertDialogDatePicker.dismiss();
                break;
        }
    }
}

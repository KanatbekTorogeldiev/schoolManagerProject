package com.example.torogeldiev.schoolmanager.entity.city;

import com.example.torogeldiev.schoolmanager.entity.country.Country;
import com.example.torogeldiev.schoolmanager.entity.region.Region;

/**
 * Created by torogeldiev on 15.02.2018.
 */

public class City {
    private int id;
    private String name;
    private Country country;
    private Region region;
    private Double longitude;
    private Double latitude;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
}

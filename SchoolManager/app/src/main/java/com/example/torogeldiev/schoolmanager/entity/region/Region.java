package com.example.torogeldiev.schoolmanager.entity.region;

/**
 * Created by torogeldiev on 15.02.2018.
 */

public class Region {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

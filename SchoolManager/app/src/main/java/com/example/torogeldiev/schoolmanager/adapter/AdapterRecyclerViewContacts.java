package com.example.torogeldiev.schoolmanager.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.torogeldiev.schoolmanager.R;
import com.example.torogeldiev.schoolmanager.adapter.holder.ContactPersonViewHolder;
import com.example.torogeldiev.schoolmanager.adapter.holder.ProgressBarViewHolder;
import com.example.torogeldiev.schoolmanager.api.OnLoadMoreListener;
import com.example.torogeldiev.schoolmanager.entity.object.Objec;
import com.example.torogeldiev.schoolmanager.screen.map.MapActivity;
import com.example.torogeldiev.schoolmanager.screen.teacher_blank_meet_or_not_Activity.TeacherBlankMeetOrNotActivity;

import java.util.List;

/**
 * Created by torogeldiev on 14.02.2018.
 */

public class AdapterRecyclerViewContacts extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Objec> list;

    //Values for load more
    private static final int VIEW_TYPE_LOADING = 1;
    private static final int VIEW_TYPE_ITEM = 0;
    private OnLoadMoreListener onLoadMoreListener;

    public AdapterRecyclerViewContacts(Context context, List<Objec> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_contacts, parent, false);
            return new ContactPersonViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(context)
                    .inflate(R.layout.item_progressbar, parent, false);
            return new ProgressBarViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ContactPersonViewHolder) {
            final ContactPersonViewHolder contactPersonViewHolder =
                    (ContactPersonViewHolder) holder;
            final Objec objec = list.get(position);
            if (objec != null) {
                contactPersonViewHolder.badge.setImageDrawable(context.getResources().getDrawable(R.drawable.circleactive));
                contactPersonViewHolder.nameTeacher.setText(objec.getFullName() != null ? objec.getFullName() : "-");
                contactPersonViewHolder.address.setText(objec.getCity().getName());
                contactPersonViewHolder.telephone.setText(objec.getTelephone());
                contactPersonViewHolder.organization.setText(objec.getPartner().getName());
                contactPersonViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, TeacherBlankMeetOrNotActivity.class);
                        intent.putExtra("object", objec.getFullName());
                        //intent.putExtra("object_full", objec);
                        intent.putExtra("partnerId", objec.getPartner().getId());
                        intent.putExtra("managerId", objec.getManager().getId());
                        intent.putExtra("contactPersonId", objec.getId());
                        intent.putExtra("organization", objec.getPartner().getName());
                        intent.putExtra("city", objec.getPartner().getCity().getName());
                        intent.putExtra("telephone", objec.getTelephone());
                        intent.putExtra("longitude", objec.getPartner().getLongitude());
                        intent.putExtra("latitude", objec.getPartner().getLatitude());
                        context.startActivity(intent);
                    }
                });
                contactPersonViewHolder.image_phone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String toDial = "tel:" + objec.getTelephone();
                        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(toDial));
                        try {
                            context.startActivity(callIntent);
                        } catch (SecurityException ex) {
                            ex.printStackTrace();
                        }
                    }
                });
            }
        } else if (holder instanceof ProgressBarViewHolder) {
            ProgressBarViewHolder progressBarViewHolder = (ProgressBarViewHolder) holder;
            progressBarViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public OnLoadMoreListener getOnLoadMoreListener() {
        return onLoadMoreListener;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public List<Objec> getList() {
        return list;
    }

    public void setList(List<Objec> list) {
        this.list = list;
    }
}

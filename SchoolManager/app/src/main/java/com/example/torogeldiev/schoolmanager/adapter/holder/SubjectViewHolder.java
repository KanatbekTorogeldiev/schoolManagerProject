package com.example.torogeldiev.schoolmanager.adapter.holder;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.torogeldiev.schoolmanager.R;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Iolana on 21.02.2018.
 */

public class SubjectViewHolder extends RecyclerView.ViewHolder{

    @BindView(R.id.item_list_subject_subject)
    public TextView subject;
    @BindView(R.id.item_list_subject_checkbox)
    public CheckBox checkSubject;
    @BindView(R.id.item_list_subject_ll)
    public LinearLayout ll;


    public SubjectViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
